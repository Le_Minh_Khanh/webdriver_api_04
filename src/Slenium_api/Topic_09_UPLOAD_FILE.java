package Slenium_api;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Topic_09_UPLOAD_FILE {
	WebDriver driver;
	String workingDirectory = System.getProperty("user.dir");
	String filebath = workingDirectory + "\\pic\\mmmm.jpg";
	String fileName = "mmmm.jpg";
	String filebath1 = workingDirectory + "\\pic\\workingDirectory";
	String fildeName1 = "sadasd.jpg";
	String folderfile = "folder" + randomNumber();
	String name = "name" + randomNumber();
	String mail = "name" + randomNumber() + "@gmail.com";
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		// driver = new ChromeDriver();
		// System.setProperty("webdriver.ie.driver", ".\\Driver\\IEDriverServer.exe");
		// driver = new InternetExplorerDriver();

		driver.manage().window().maximize();

	}

	public void TC01_Uploadfilebysendkeys() throws Exception {
		driver.get("http://blueimp.github.io/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement addFile = driver.findElement(By.xpath("//input[@type='file']"));
		addFile.sendKeys(filebath);
		Thread.sleep(300);
		WebElement uploadFile = driver.findElement(By.xpath("//p[@class='name' and text()='" + fileName + "']"));
		Assert.assertTrue(uploadFile.isDisplayed());

	}

	public void TC_02_UploadfilebyAutoIT() throws Exception {
		driver.get("http://blueimp.github.io/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// WebElement upfileWithFirefox =
		// driver.findElement(By.xpath("//input[@type='file']"));
		// Lay the Css trong web.
		WebElement upfileWithchrome = driver.findElement(By.cssSelector(".fileinput-button>input"));

		ClickJS(upfileWithchrome);
		Runtime.getRuntime().exec(new String[] { ".\\Auto_IT\\firefox.exe", filebath });
		WebElement uploadFile1 = driver.findElement(By.xpath("//p[@class='name' and text()='" + fileName + "']"));
		Assert.assertTrue(uploadFile1.isDisplayed());
	}

	
	public void TC_03_UploadfilebyRobotclass() throws Exception {
		driver.get("http://blueimp.github.io/jQuery-File-Upload/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// Specify the file location with extension
		StringSelection select = new StringSelection(filebath);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click
		driver.findElement(By.className("fileinput-button")).click();

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		WebElement uploadFile1 = driver.findElement(By.xpath("//p[@class='name' and text()='" + fileName + "']"));
		Assert.assertTrue(uploadFile1.isDisplayed());
	}

	@Test
	public void TC_04_Uploadfile() {
//step1
		driver.get("https://encodable.com/uploaddemo/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//step2
		WebElement upFile1 = driver.findElement(By.xpath("//input[@id='uploadname1']"));
		upFile1.sendKeys(filebath);
		//step 3
		WebElement chooseFolder = driver.findElement(By.xpath(
				"//select[@class='upform_field picksubdir_field' and option[text()='/uploaddemo/files/a_01/']]"));
		chooseFolder.click();
		// Step 4,5,6 subfolder
		WebElement subFolder = driver.findElement(By.xpath("//input[@id='newsubdir1']"));
		subFolder.sendKeys(folderfile);
		WebElement mailAddress = driver.findElement(By.xpath("//input[@id='formfield-email_address']"));
		mailAddress.sendKeys(mail);
		WebElement fisrtName = driver.findElement(By.xpath("//input[@id='formfield-first_name']"));
		fisrtName.sendKeys(name);
		WebElement submitButton = driver.findElement(By.xpath("//input[@id='uploadbutton']"));
		submitButton.click();
		//WebElement upFile2 = driver.findElement(By.xpath("//input[@id='uploadname2']"));
		//upFile2.sendKeys(filebath1);
		//Wait page xuat hien:
				//By waitpage = By.xpath("//div[@id='progBar']");
		//wait.until(ExpectedConditions.invisibilityOfElementLocated(waitpage));
		//step 7 		Verify mail
				WebElement verifyMailaddress = driver.findElement(By.xpath("//dl[@id='fcuploadsummary']/dd[text()='Email Address: "+mail+"']"));
		Assert.assertTrue(verifyMailaddress.isDisplayed());
		
		//verify subfolder
		WebElement verifyFolder = driver.findElement(By.xpath("//dl[@id='fcuploadsummary']/dd[text()='First Name: "+name+"']"));
		Assert.assertTrue(verifyFolder.isDisplayed());
		
		// Verify file name
		WebElement verifynamefile = driver.findElement(By.xpath("//a[text()='"+fileName+"']"));
		Assert.assertTrue(verifynamefile.isDisplayed());
		
		// step 8 click view upload
		WebElement viewUpload = driver.findElement(By.xpath("//a[text()='View Uploaded Files']"));
		viewUpload.click();
		
		// step 9 click folder file 
		WebElement checkfolder = driver.findElement(By.xpath("//a[text()='"+folderfile+"']"));
		checkfolder.click();
		// step 10 verify image sau khi upload
		WebElement verifyimagefolder = driver.findElement(By.xpath("//img[@alt='thumbnail image for "+fileName+"']"));
		verifyimagefolder.isDisplayed();
		
			

	}

	public Object ClickJS(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(999999);
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
