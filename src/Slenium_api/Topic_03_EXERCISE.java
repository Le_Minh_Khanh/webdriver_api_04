package Slenium_api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_03_EXERCISE {
	WebDriver driver;

	@Test
	public void TC1_2_3Topic03() {
		// Firefox
		driver = new FirefoxDriver();

		// Chrome
		// System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		// driver = new ChromeDriver();
		// IE

		// System.setProperty("webdriver.ie.driver", ".\\Drivers\\IEDriverServer.exe");
		// WebDriver driver = new InternetExplorerDriver();
		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		String emailTC1 = "//*[@id='mail']";
		isElementDisplayed(emailTC1);
		String emailEnable = "//*[@id='mail']";
		isElementEnable(emailEnable);
		String passDisable = "//input[@id='password']";
		isElementDisabled(passDisable);
		String emailXpath = ("//label[@for='under_18']");
		isElementSelected(emailXpath);
	}
//TC01
	public void isElementDisplayed(String emailTC1) {

		WebElement mail = driver.findElement(By.xpath(emailTC1));
		if (mail.isDisplayed()) {
			System.out.println("Element" + mail + "email Is Enable");

		} else {
			mail.sendKeys("Automation Testing");
			System.out.println("Element" + mail + "email Is disable");
		}

		WebElement Tickbox = driver.findElement(By.xpath("//label[@for='under_18']"));
		if (Tickbox.isDisplayed()) {
			System.out.println("Element" + Tickbox + "Tickbox Is Enable");

		} else {
			System.out.println("Element" + Tickbox + "Tickbox Is disable");
		}
		WebElement Education = driver.findElement(By.xpath("//textarea[@name='user_bio']"));
		if (Education.isDisplayed()) {
			System.out.println("Element" + Education + "Education Is Enable");

		} else {
			mail.sendKeys("Automation Testing");
			System.out.println("Element" + Education + "Education Is disable");
		}
	}
	//TC02
	public void isElementEnable(String emailEnable) {

		WebElement email = driver.findElement(By.xpath(emailEnable));
		if (email.isEnabled()) {
			System.out.println("Element" + emailEnable + "email Is Enable");
			email.sendKeys("Automation Testing");
		} else {

			System.out.println("Element" + emailEnable + "email Is not Enable");
		}

		WebElement element1 = driver.findElement(By.xpath("//textarea[@name='user_edu']"));
		if (element1.isEnabled()) {
			System.out.println("Element" + emailEnable + "text box Is Enable");

		} else {
			System.out.println("Element" + emailEnable + "texboxIs Disable");

		}

		WebElement element2 = driver.findElement(By.xpath("//label[@for='under_18']"));
		if (element2.isEnabled()) {
			System.out.println("Element" + emailEnable + "age checkbox Is Enable");
		} else {
			System.out.println("Element" + emailEnable + "age checkboxIs Disable");

		}

		WebElement element3 = driver.findElement(By.xpath("//select[@id='job1']"));
		if (element3.isEnabled()) {
			System.out.println("Element" + emailEnable + "Dropdown List is enabled");
		} else {
			System.out.println("Element" + emailEnable + "Dropdown List is Disabled");

		}

		WebElement element4 = driver.findElement(By.xpath("//input[@id='slider-1']"));
		if (element4.isEnabled()) {
			System.out.println("Element" + emailEnable + "Slide is enabled");
		} else {
			System.out.println("Element" + emailEnable + "Slide is Disabled");

		}

		WebElement element5 = driver.findElement(By.xpath("//button[@id='button-enabled']"));
		if (element5.isEnabled()) {
			System.out.println("Element" + emailEnable + "Button is enabled");
		} else {
			System.out.println("Element" + emailEnable + "Button is Disabled");

		}

	}

	public void isElementDisabled(String passDisable) {
		WebElement element = driver.findElement(By.xpath(passDisable));
		if (element.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}

		WebElement element1 = driver.findElement(By.xpath("//label[@for='radio-disabled']"));
		if (element1.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}

		WebElement element2 = driver.findElement(By.xpath("//label[@for='radio-disabled']"));
		if (element2.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}
		WebElement element3 = driver.findElement(By.xpath("//textarea[@id='bio']"));
		if (element3.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}
		WebElement element4 = driver.findElement(By.xpath("//option[@value='dropdown-disabled']"));
		if (element4.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}
		WebElement element5 = driver.findElement(By.xpath("//label[@for='check-disbaled']"));
		if (element5.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}
		WebElement element6 = driver.findElement(By.xpath("//button[@id='button-disabled']"));
		if (element6.isEnabled()) {
			System.out.println("Element" + passDisable + "Is Disable");

		} else {
			System.out.println("Element" + passDisable + "Is  Enable");
		}

	}
	//TC03
	public void isElementSelected(String emailXpath)  {
		// Select Under 18
		WebElement Age = driver.findElement(By.xpath("//label[@for='under_18']"));
		Age.click();
		
		if (Age.isDisplayed()) {
			System.out.println("Element" + Age + "Age Is Select");

		} else {
			Age.click();
			System.out.println("Element" + Age + "Age Is not Select");

		}
		// Select Interests (Development)
		WebElement Interests = driver.findElement(By.xpath("//label[@for='development']"));
		Interests.click();
		
		if (Interests.isDisplayed()) {
			System.out.println("Element" + Interests + "Interests Is Select");

		} else {
			Interests.click();
			System.out.println("Element" + Interests + "Interests Is not Select");
		}
	}
	
	@AfterClass
	public void afterClass() {
		driver.quit();
	}
}