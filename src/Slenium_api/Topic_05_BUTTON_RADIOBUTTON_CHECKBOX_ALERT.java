package Slenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_05_BUTTON_RADIOBUTTON_CHECKBOX_ALERT {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.manage().window().maximize();
		// driver.get("http://live.guru99.com/");
		// System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		// driver = new ChromeDriver();

	}

	public void TC_01_clickwithJavaScrip() {
		driver.get("http://live.guru99.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]")).click();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/login/", driver.getCurrentUrl());
		WebElement creareAcount = driver.findElement(By.xpath("//span[text()='Create an Account']"));
		clickElementByJavascript(creareAcount);
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", driver.getCurrentUrl());
	}

	public void TC_02_Checkbox() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/checkboxes");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement CheckDual_Zone = driver.findElement(By.xpath("//label[text()='Dual-zone air conditioning']/preceding-sibling::input"));
		clickElementByJavascript(CheckDual_Zone);
		Assert.assertTrue(CheckDual_Zone.isSelected());
		uncheckThetextbox(CheckDual_Zone);

	}

	public void TC_03_radiobutton() {
		driver.get("http://demos.telerik.com/kendo-ui/styling/radios");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement Checkradiobutton = driver.findElement(By.xpath("//label[text()='2.0 Petrol, 147kW']/preceding-sibling::input"));
		clickElementByJavascript(Checkradiobutton);
		Assert.assertTrue(Checkradiobutton.isSelected());

	}

	public void TC_04_ClickJSAlert() {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement alertClick = driver.findElement(By.xpath("//button[text()='Click for JS Alert']"));
		alertClick.click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		WebElement textsuccess = driver.findElement(By.xpath("//p[@id='result']"));
		Assert.assertEquals("You clicked an alert successfully", textsuccess.getText());

	}

	public void TC_05_Click_JS_Confirm() {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement alertClick = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
		alertClick.click();
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
		WebElement textsuccess = driver.findElement(By.xpath("//p[@id='result']"));
		Assert.assertEquals("You clicked: Cancel", textsuccess.getText());

	}

	@Test
	public void TC_05_Click_JS_Prompt() {
		String name = "Le Minh Khanh";

		driver.get("http://daominhdam.890m.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement alertClick = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
		alertClick.click();
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(name);
		alert.accept();
		WebElement textsuccess = driver.findElement(By.xpath("//p[@id='result']"));
		Assert.assertEquals("You entered: " + name, textsuccess.getText());

	}

	public void clickElementByJavascript(WebElement element) {
		JavascriptExecutor je = (JavascriptExecutor) driver;
		je.executeScript("arguments[0].click();", element);
	}

	public void uncheckThetextbox(WebElement element) {
		if (element.isSelected()) {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].click();", element);
			Assert.assertTrue(!element.isSelected());
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
