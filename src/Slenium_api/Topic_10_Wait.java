package Slenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

import org.testng.annotations.BeforeClass;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Topic_10_Wait {
	WebDriver driver;
	WebDriverWait wait;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		 //System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		 //driver = new ChromeDriver();
		// System.setProperty("webdriver.ie.driver", ".\\Driver\\IEDriverServer.exe");
		// driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 30);

	}

	
	public void TC_Implicit_Wait() {

		driver.get("http://the-internet.herokuapp.com/dynamic_loading/2");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		WebElement clickStartBT = driver.findElement(By.xpath("//*[@id='start']/button"));
		clickStartBT.click();
		WebElement textHello = driver.findElement(By.xpath("//div[@id='finish']/h4"));

		Assert.assertEquals("Hello World", textHello.getText());
	}

	@Test
	public void TC02_Explicit_Wait() {

		driver.get(
				"http://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
		WebElement textdate = driver.findElement(By.xpath("//span[@class='label']"));
		wait.until(ExpectedConditions.visibilityOf(textdate));

		WebElement checkNumber = driver.findElement(By.xpath("//a[text()='22']"));
		checkNumber.click();

		By loadingdisappear = By.xpath("//div[@class='raDiv']");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingdisappear));
		
		WebElement numberselect = driver.findElement(By.xpath("//td[@class='rcSelected']/a[text()='22']"));
		wait.until(ExpectedConditions.visibilityOf(numberselect));
		
		WebElement numberDisplays = driver.findElement(By.xpath("//span[@id='ctl00_ContentPlaceholder1_Label1']"));
		wait.until(ExpectedConditions.visibilityOf(numberDisplays));
		String number = numberDisplays.getText().trim();
		Assert.assertEquals(number, "Friday, June 22, 2018");

	}
	
	public void TC03_Fluent_Wait_01() {
		driver.get("https://stuntcoders.com/snippets/javascript-countdown/");
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		WebElement countdount = driver.findElement(By.xpath("//div[@id='javascript_countdown_time']"));
		wait.until(ExpectedConditions.visibilityOf(countdount));
		new FluentWait<WebElement>(countdount).withTimeout(15, TimeUnit.SECONDS)
		.pollingEvery(1, TimeUnit.SECONDS)
		.ignoring(NoSuchElementException.class).until(new Function<WebElement, Boolean>() {
		
			public Boolean apply(WebElement element) {
				boolean flag = element.getText().endsWith("00");
				System.out.println("time =" + element.getText());
				return flag;
			}
			
		});
	}
		
	
	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
