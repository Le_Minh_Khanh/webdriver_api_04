package Slenium_api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_08_JS {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		 driver = new FirefoxDriver();
		//System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		//driver = new ChromeDriver();
		//System.setProperty("webdriver.ie.driver", ".\\Driver\\IEDriverServer.exe");
		//driver = new InternetExplorerDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void TC_01Javascript_Excecutor() {

		driver.get("http://live.guru99.com/");
		String domain = (String) executeJSForBrowserElement("return document.domain;");
		Assert.assertEquals("live.guru99.com", domain);

		String ulr = (String) executeJSForBrowserElement("return document.URL;");
		Assert.assertEquals("http://live.guru99.com/", ulr);

		WebElement mobileButton = driver.findElement(By.xpath("//a[text()='Mobile']"));
		executeclickForWebElement(mobileButton);

		WebElement addtoCart = driver.findElement(By.xpath("//h2[@class='product-name']/a[@title='Samsung Galaxy']/../following-sibling::div[@class='actions']/button"));
		executeclickForWebElement(addtoCart);

		String addtocarttext = (String) executeJSForBrowserElement("return document.documentElement.innerText;");
		Assert.assertTrue(addtocarttext.contains("Samsung Galaxy was added to your shopping cart."));

		WebElement policylink = driver.findElement(By.xpath("//a[text()='Privacy Policy']"));
		executeclickForWebElement(policylink);

		String policypage = (String) executeJSForBrowserElement("return document.title");
		Assert.assertEquals(policypage, "Privacy Policy");
		scrollToBottomPage();

		WebElement whitelist = driver.findElement(By.xpath("//th[text()='WISHLIST_CNT']/following-sibling::td[text()='The number of items in your Wishlist.']"));
		Assert.assertTrue(whitelist.isDisplayed());

		String gurupage = "http://demo.guru99.com/v4/";
		executeJSForBrowserElement("  window.location = ' " + gurupage +  "  '   ");

		String domainGuru = (String) executeJSForBrowserElement("return document.domain;");
		Assert.assertEquals(domainGuru, "demo.guru99.com");

	}

	@Test
	public void TC_02() {
		driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_disabled");
		WebElement frameTextbox = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(frameTextbox);
		WebElement enableTextbox = driver.findElement(By.xpath("//input[@name='lname']"));
		removeAttributeInDOM(enableTextbox, "disabled");
		sendkeyToElementByJS(enableTextbox, "Le minh");
		WebElement inputTextbox = driver.findElement(By.xpath("//input[@name='fname']"));
		sendkeyToElementByJS(inputTextbox, "khanh");
		WebElement clicksumit = driver.findElement(By.xpath("//input[@type='submit']"));
		clicksumit.click();
		driver.switchTo().defaultContent();
		WebElement frameresult = driver.findElement(By.xpath("//iframe[@id='iframeResult']"));
		driver.switchTo().frame(frameresult);
		WebElement verifytext = driver.findElement(By.xpath("//div[contains(text(),'Le minh')]"));
		Assert.assertTrue(verifytext.isDisplayed());

	}

	public Object executeJSForBrowserElement(String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeclickForWebElement(WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object removeAttributeInDOM(WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public void sendkeyToElementByJS(WebElement element, String value) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('value', '" + value + "')", element);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
