package Slenium_api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test_Template_enviroment {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://live.guru99.com/");
	}

	@Test
	public void TC_01_checkTittle() {
		String homePageTitle = driver.getTitle();
		Assert.assertEquals("Home page", homePageTitle);

	}

	@Test
	public void TC_02_CheckURL() {
		String homePageULR = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/", homePageULR);
	}

	@AfterClass
	public void afterClass() {
	driver.quit();
	}

}
