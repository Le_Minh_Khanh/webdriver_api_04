package Slenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Topic_04_dropdown_textbox_textarea {
	WebDriver driver;
	String ID, CustommerName, addr;
	private String mailcreate;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		CustommerName = "Minhkhanh";
		addr = "1234 Trung Nu Vuong";
		mailcreate = ("automation" + randomNumber() + "@gmail.com");
	}
	public void TC_01_Dropdown() throws Exception {

		driver.get("http://daominhdam.890m.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		Select TextDropDown = new Select(driver.findElement(By.xpath("//select[@id='job1']")));
		Assert.assertFalse(TextDropDown.isMultiple());
		TextDropDown.selectByVisibleText("Automation Tester");
		Thread.sleep(2000);
		Assert.assertEquals("Automation Tester", TextDropDown.getFirstSelectedOption().getText());
		TextDropDown.selectByValue("manual");
		Thread.sleep(2000);
		Assert.assertEquals("Manual Tester", TextDropDown.getFirstSelectedOption().getText());
		TextDropDown.selectByIndex(3);
		Thread.sleep(2000);
		Assert.assertEquals("Mobile Tester", TextDropDown.getFirstSelectedOption().getText());
		Assert.assertEquals(5, TextDropDown.getOptions().size());
	}
	@Test
	public void TC02_Textboxtextarea() throws Exception {
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		// login
		WebElement Username = driver.findElement(By.xpath("//input[@name='uid']"));
		Username.sendKeys("mngr135167");

		WebElement Userpass = driver.findElement(By.xpath("//input[@name='password']"));
		Userpass.sendKeys("hYdypUh");
		WebElement Login = driver.findElement(By.xpath("//input[@name='btnLogin']"));
		Login.click();
		WebElement Successtext = driver.findElement(By.xpath("//marquee[@class=\"heading3\"]"));
		Successtext.getText();
		Assert.assertTrue(Successtext.isDisplayed());
		WebElement NewCustommer = driver.findElement(By.xpath("//a[text()='New Customer']"));
		NewCustommer.click();
		WebElement Custommeruser = driver.findElement(By.xpath("//input[@name='name']"));
		Custommeruser.sendKeys(CustommerName);
		WebElement Gender = driver.findElement(By.xpath("//input[@value='m']"));
		Gender.click();
		WebElement Birthday = driver.findElement(By.xpath("//input[@name='dob']"));
		Birthday.sendKeys("23/10/1987");
		WebElement Address = driver.findElement(By.xpath("//textarea[@name='addr']"));
		Address.sendKeys(addr);
		WebElement City = driver.findElement(By.xpath("//input[@name='city']"));
		City.sendKeys("Da Nang");
		WebElement State = driver.findElement(By.xpath("//input[@name='state']"));
		State.sendKeys("Hai chau");
		WebElement Pin = driver.findElement(By.xpath("//input[@name='pinno']"));
		Pin.sendKeys("123456");
		WebElement Mobile = driver.findElement(By.xpath("//input[@ name='telephoneno']"));
		Mobile.sendKeys("0123456789");
		WebElement Email = driver.findElement(By.xpath("//input[@ name='emailid']"));
		Email.sendKeys(mailcreate);
		WebElement Pass = driver.findElement(By.xpath("//input[@name='password']"));
		Pass.sendKeys("123456");
		WebElement Submit = driver.findElement(By.xpath("//input[@value='Submit']"));
		Submit.click();

		WebElement ResignSuccesstext = driver.findElement(By.xpath("//p[@class='heading3' and text()='Customer Registered Successfully!!!']"));
				Assert.assertTrue(ResignSuccesstext.isDisplayed());
		
		//Step 05 
		ID = driver.findElement(By.xpath("//td[text()='Customer ID']/following-sibling::td")).getText();
		
		// Step 06 
		WebElement EditCS = driver.findElement(By.xpath("//a[text()='Edit Customer']"));
		EditCS.click();
		Thread.sleep(3000);
		WebElement CustomerID = driver.findElement(By.xpath("//input[@name='cusid']"));
		CustomerID.sendKeys(ID);
				WebElement SubmitID = driver.findElement(By.xpath("//input[@name='AccSubmit']"));
		SubmitID.click();
		
				// Step 07
		WebElement CustomerEdit = driver.findElement(By.xpath("//input[@name='name']"));
				Assert.assertEquals(CustommerName, CustomerEdit.getAttribute("value"));
				WebElement AddressEdit = driver.findElement(By.xpath("//textarea[@name='addr']"));
		Assert.assertEquals(addr, AddressEdit.getAttribute("value"));

		
		WebElement Addr = driver.findElement(By.xpath("//textarea[@name='addr']"));
		Addr.clear();
		
		Addr.sendKeys("123 tam ky");
		Assert.assertEquals("123 tam ky", Addr.getAttribute("value"));
		
		
		WebElement Cityrename = driver.findElement(By.xpath("//input[@name='city']"));
		Cityrename.clear();
				Cityrename.sendKeys("Da nang");
		Assert.assertEquals("Da nang", Cityrename.getAttribute("value"));
		
		
	}

	public int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(999999);
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
