package Slenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_02_EXERCISE_WEBDRIVER {
	WebDriver driver;
	private String mailcreate;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://live.guru99.com/");
		mailcreate = ("automation" + randomNumber() + "@gmail.com");
	}

	@Test
	public void TC_01() {

		String PageTitle = driver.getTitle();
		Assert.assertEquals("Home page", PageTitle);
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]")).click();
		driver.findElement(By.xpath("//a[@class='button']")).click();
		driver.navigate().back();
		String PageULR = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/login/", PageULR);
		driver.navigate().forward();
		String CreateAccPage = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/customer/account/create/", CreateAccPage);
	}

	@Test
	public void TC_02_CheckURL() {
		driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
		driver.findElement(By.xpath("//button[@id=\"send2\"]")).click();
		String Required = driver.findElement(By.xpath("//p[@class='required']")).getText();
		Assert.assertEquals(Required, "Required Fields");
		String emailErrorMsg = driver.findElement(By.xpath("//div[@id='advice-required-entry-email']")).getText();
		Assert.assertEquals(emailErrorMsg, "Please enter a valid email address. For example johndoe@domain.com.");
		String passErrorMsg = driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals(passErrorMsg, "This is a required field.");
	}

	@Test
	public void TC_03_LoginwithEmailinvalid() {

		driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("123434234@12312.123123");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("");
		driver.findElement(By.xpath("//button[@id=\"send2\"]")).click();
		String passErrorMsg = driver.findElement(By.xpath("//div[@id='advice-required-entry-pass']")).getText();
		Assert.assertEquals(passErrorMsg, "This is a required field.");
	}

	@Test
	public void TC_04LoginwithPasswordincorrect() {

		driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]")).click();
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("automation@gmail.com");
		driver.findElement(By.xpath("//input[@id='pass']")).sendKeys("123");
		driver.findElement(By.xpath("//button[@id=\"send2\"]")).click();
		String passErrorMsg = driver.findElement(By.xpath("//div[@id='advice-validate-password-pass']")).getText();
		Assert.assertEquals(passErrorMsg, "Please enter 6 or more characters without leading or trailing spaces.");

	}

	@Test

	public void TC_05_Create_An_Account() throws InterruptedException {

		WebElement MyAccount = driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]"));
		MyAccount.click();
		WebElement CreateAcount = driver.findElement(By.xpath("//a[@class='button']"));
		CreateAcount.click();
		driver.findElement(By.xpath("//input[@id='firstname']")).sendKeys("fasdssdsd");
		driver.findElement(By.xpath("//input[@title='Middle Name/Initial']")).sendKeys("fdsssad");
		driver.findElement(By.xpath("//input[@id='lastname']")).sendKeys("agdsssas");
		WebElement EmailAccount = driver.findElement(By.xpath("//input[@id='email_address']"));
		EmailAccount.sendKeys(mailcreate);
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("1234566");
		driver.findElement(By.xpath("//input[@id='confirmation']")).sendKeys("1234566");
		driver.findElement(By.xpath("//input[@id='is_subscribed']")).click();
		driver.findElement(By.xpath("//button[@class='button' and @title='Register'] ")).click();
		WebElement registerSuccessMsg = driver.findElement(By.xpath("//span[text()='Thank you for registering with Main Website Store.']"));
		Assert.assertTrue(registerSuccessMsg.isDisplayed());
		driver.findElement(By.xpath("//a[@class='skip-link skip-account'] // span[@class='label']")).click();
		driver.findElement(By.xpath("//a[@title='Log Out']")).click();
		Thread.sleep(5000);
		String homePageTitle = driver.getCurrentUrl();
		Assert.assertEquals("http://live.guru99.com/index.php/", homePageTitle);
	}

	public int randomNumber() {
		Random random = new Random();
		int number = random.nextInt(999999);
		return number;
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
