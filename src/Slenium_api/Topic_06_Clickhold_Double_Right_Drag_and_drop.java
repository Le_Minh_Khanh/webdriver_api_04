package Slenium_api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Topic_06_Clickhold_Double_Right_Drag_and_drop {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		// driver = new FirefoxDriver();
		System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		// driver.manage().window().maximize();
		// driver.get("http://live.guru99.com/");
	}

	public void TC_01_Hover() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://daominhdam.890m.com/");
		WebElement hovermouse = driver.findElement(By.xpath("//a[text()='Hover over me']"));
		Actions action = new Actions(driver);
		action.moveToElement(hovermouse).perform();
		WebElement Checkhover = driver.findElement(By.xpath("//div[@class='tooltip-inner' and contains(text(),'Hooray!')] "));
		Assert.assertTrue(Checkhover.isDisplayed());

	}

	public void TC_01_case2_Hover() throws Exception {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.myntra.com/");
		WebElement hoverLogin = driver.findElement(By.xpath("//div[@class='desktop-userIconsContainer']"));
		Actions Hovermouse = new Actions(driver);
		Hovermouse.moveToElement(hoverLogin);
		WebElement clicklogin = driver.findElement(By.xpath("//a[@data-track='login']"));
		Hovermouse.click(clicklogin).perform();
		Thread.sleep(3000);
		WebElement loginForm = driver.findElement(By.xpath("//div[@class='login-box']"));
		Assert.assertTrue(loginForm.isDisplayed());

	}

	public void TC_02Click_and_hold() throws Exception {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<WebElement> Listnumber = driver.findElements(By.xpath("//ol[@class='ui-selectable']/li"));
		Actions ClickHold = new Actions(driver);
		ClickHold.clickAndHold(Listnumber.get(0)).moveToElement(Listnumber.get(2)).release().perform();
		List<WebElement> Listnumberchoose = driver.findElements(By.xpath("//ol[@class='ui-selectable']/li[@class='ui-state-default ui-selectee ui-selected']"));
		int number = Listnumberchoose.size();
		Assert.assertEquals(3, number);

	}

	public void TC_02Click_and_holdrandom() throws Exception {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/resources/demos/selectable/display-grid.html");
		List<WebElement> Listnumber2 = driver.findElements(By.xpath("//ol[@class='ui-selectable']/li"));
		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL).build().perform();
		Listnumber2.get(0).click();
		Listnumber2.get(4).click();
		Listnumber2.get(9).click();
		Listnumber2.get(10).click();
		action.keyUp(Keys.CONTROL).build().perform();
		List<WebElement> Listnumberchoose2 = driver.findElements(By.xpath("//ol[@class='ui-selectable']/li[@class='ui-state-default ui-selectee ui-selected']"));
		int number = Listnumberchoose2.size();
		Assert.assertEquals(4, number);

	}

	public void TC_03_DoubleClick() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://www.seleniumlearn.com/double-click");
		WebElement Double = driver.findElement(By.xpath("//button[text()='Double-Click Me!']"));
		Actions action = new Actions(driver);
		action.doubleClick(Double).perform();
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("The Button was double-clicked.", alert.getText());
		alert.accept();
	}

	
	public void TC_04_RightClick() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://swisnl.github.io/jQuery-contextMenu/demo.html");
		WebElement Rclick = driver.findElement(By.xpath("//span[text()='right click me']"));
		Actions action = new Actions(driver);
		action.contextClick(Rclick).perform();
		// trc khi hover
		WebElement Quit = driver.findElement(By.xpath("//li[contains(@class,'context-menu-icon-quit')]/span[text()='Quit']"));
		action.moveToElement(Quit).perform();
		// sau khi hover
		WebElement hoverQuit = driver.findElement(By.xpath("//li[contains(@class,'context-menu-visible') and contains(@class,'context-menu-hover')]/span[text()='Quit']"));
		Assert.assertTrue(hoverQuit.isDisplayed());
		action.click(Quit).perform();
		Alert alert = driver.switchTo().alert();
		Assert.assertEquals("clicked: quit", alert.getText());
		alert.accept();

	}
	
	public void TC_05_Drag_and_drop() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://demos.telerik.com/kendo-ui/dragdrop/angular");
		WebElement soucreTarget = driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement Target = driver.findElement(By.xpath("//div[@id='droptarget']"));
		Actions action = new Actions(driver);
		action.dragAndDrop(soucreTarget , Target).release().perform();
		WebElement afterDrop = driver.findElement(By.xpath("//div[text()='You did great!']"));
		Assert.assertTrue(afterDrop.isDisplayed());
	
	}
	@Test
	public void TC_05_2_Drag_and_drop() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
		WebElement soucreTarget1 = driver.findElement(By.xpath("//div[@class='ui-widget-content ui-draggable ui-draggable-handle']"));
		WebElement Target1 = driver.findElement(By.xpath("//div[@class='ui-widget-header ui-droppable']"));
		Actions action = new Actions(driver);
		action.dragAndDrop(soucreTarget1 , Target1).release().perform();
		WebElement afterDrop1 = driver.findElement(By.xpath("//div[@class='ui-widget-header ui-droppable ui-state-highlight']"));
		Assert.assertEquals("Dropped!", afterDrop1.getText());
	
	}
	
		
		
	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
