package Slenium_api;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Topic_07_Iframe_frame_Windowpopup {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		 driver = new FirefoxDriver();
		//System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
		//driver = new ChromeDriver();

	}

	public void TC_01closePopup_ifream() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.hdfcbank.com/");
		// Step 1
		List<WebElement> iframepopup = driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));

		if (iframepopup.size() > 0) {

			WebElement notificationPopup = driver.findElement(By.xpath("//iframe[@id='vizury-notification-template']"));
			driver.switchTo().frame(notificationPopup);
			WebElement closePopup = driver.findElement(By.xpath("//div[@id='div-close']"));
			closePopup.click();
		}
		// Step3
		WebElement textbanner = driver.findElement(By.xpath("//div[@class='flipBannerWrap']//iframe"));
		driver.switchTo().frame(textbanner);
		WebElement lookingfortext = driver.findElement(By.xpath("//span[@id='messageText']"));
		Assert.assertEquals(lookingfortext.getText(), "What are you looking for?");
		driver.switchTo().defaultContent();
		// Step 4
		WebElement Imagebanner = driver.findElement(By.xpath("//div[@class='slidingbanners']//iframe"));
		driver.switchTo().frame(Imagebanner);
		List<WebElement> ListbannerDisplay = driver.findElements(By.xpath("//img[@class='bannerimage']"));
		int imagenumber = ListbannerDisplay.size();
		Assert.assertEquals(6, imagenumber);
		driver.switchTo().defaultContent();

		WebElement flipBanner = driver.findElement(By.xpath("//div[@class='flipBanner']//img"));
		flipBanner.isDisplayed();

		List<WebElement> listflipBanner = driver.findElements(By.xpath("//img[@class='front icon']"));
		imagenumber = listflipBanner.size();
		Assert.assertEquals(8, imagenumber);

	}

	public void TC_02_windowpopup() {
		// >= 2 pop up/tab
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://daominhdam.890m.com/");
		WebElement Clickbutton = driver.findElement(By.xpath("//a[text()='Click Here']"));
		Clickbutton.click();
		String parentWindowID = driver.getWindowHandle();
		switchToChildWindow(parentWindowID);
		String gooGletitle = driver.getTitle();
		Assert.assertEquals(gooGletitle, "Google");
		closeAllWithoutParentWindows(parentWindowID);

	}

	@Test
	public void TC_03() {
		// >=2 pop up/tab
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.get("http://www.hdfcbank.com/");
		// Step 01 Switch toi parent window
		String parentWindowID = driver.getWindowHandle();
		//switchToChildWindow(parentWindowID);
		// Step 02 check notification
		List<WebElement> iframePopup = driver.findElements(By.xpath("//iframe[@id='vizury-notification-template']"));

		if (iframePopup.size() > 0) {
			WebElement iframePopupsize = driver.findElement(By.xpath("//iframe[@id='vizury-notification-template']"));
			driver.switchTo().frame(iframePopupsize);
			Assert.assertTrue(iframePopupsize.isDisplayed());
			WebElement notiFica = driver.findElement(By.xpath("//div[@id='div-close']"));
			notiFica.click();
			driver.switchTo().defaultContent();
			driver.quit();
		}
		// Step 03 click link
		WebElement angriLink = driver.findElement(By.xpath("//a[text()='Agri']"));
		angriLink.click();
		// Switch toi HDFC
		switchToWindowByTitle("HDFC Bank Kisan Dhan Vikas e-Kendra");
		// Step 04 click Account
		WebElement accountDetails = driver.findElement(By.xpath("//p[text()='Account Details']"));
		accountDetails.click();
		// Switch toi accountpage
		switchToWindowByTitle("Welcome to HDFC Bank NetBanking");
		// Step 05 click PP
		WebElement framePolicy = driver.findElement(By.xpath("//frame[@name='footer']"));
		driver.switchTo().frame(framePolicy);
		WebElement policyClick = driver.findElement(By.xpath("//a[text()='Privacy Policy']"));
		policyClick.click();
		driver.switchTo().defaultContent();
		// Switch toi policy
		switchToWindowByTitle("HDFC Bank - Leading Bank in India, Banking Services, Private Banking, Personal Loan, Car Loan");
		WebElement cssClick = driver.findElement(By.xpath("//a[@title='Corporate Social Responsibility']"));
		cssClick.click();
		switchToWindowByTitle("HDFC Bank: Personal Banking Services");
						// close all page tru Win parent
		closeAllWithoutParentWindows(parentWindowID);

	}

	public void switchToChildWindow(String parent) {
		// Get all windows ID
		Set<String> allWindows = driver.getWindowHandles();

		// For-each
		for (String runWindow : allWindows) {
			System.out.println("Windows ID = " + runWindow);

			// Kiem tra ID nao ma khac Parent ID thi minh switch qa
			if (!runWindow.equals(parent)) {

				// Switch qa windows ID # parent ID
				driver.switchTo().window(runWindow);
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(String parentWindow) {
        // Get all windows ID
        Set<String> allWindows = driver.getWindowHandles();

        // Duyet qua tung ID
        for (String runWindows : allWindows) {

            // Neu ID # parent ID
            if (!runWindows.equals(parentWindow)) {
                // Switch qa id do
                driver.switchTo().window(runWindows);

                // dong ID
                driver.close();
            }
        }

        // Switch qa parent Windows ID
        driver.switchTo().window(parentWindow);
        // Kiem tra no chi con lai 1 window (parent)
        if (driver.getWindowHandles().size() == 1)
            // return lai gia tri cho ham closeAllWithoutParentWindows (nhan gia tri tra ve la boolean)
            return true;
        else
            // return lai gia tri cho ham closeAllWithoutParentWindows (nhan gia tri tra ve la boolean)
            return false;
    }

	public void switchToWindowByTitle(String title) {
		// Get all windows ID
		Set<String> allWindows = driver.getWindowHandles();

		// Duyet qua tung ID
		for (String runWindows : allWindows) {
			// Switch qa tung ID
			driver.switchTo().window(runWindows);

			// Get title cua page do ra
			String currentTitle = driver.getTitle();

			// Title current windows = title truyen vao
			if (currentTitle.equals(title)) {
				break;
			}
		}
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
