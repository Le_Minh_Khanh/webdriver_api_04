package testAnnotation;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class TC01_TestNG_DataProvider {
	WebDriver driver;

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("http://live.guru99.com/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test(dataProvider = "UserandPass")
	public void loginWithmultiAcc(String user, String pass) {
		WebElement account = driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]"));
		account.click();
		WebElement mailaddress = driver.findElement(By.xpath("//input[@id='email']"));
		mailaddress.sendKeys(user);
		WebElement passWork = driver.findElement(By.xpath("//input[@id='pass']"));
		passWork.sendKeys(pass);
		WebElement login = driver.findElement(By.xpath("//button[@id=\"send2\"]"));
		login.click();
		WebElement loginsuccess = driver.findElement(By.xpath("//h1[text()='My Dashboard']"));
		Assert.assertTrue(loginsuccess.isDisplayed());

		WebElement drobox = driver
				.findElement(By.xpath("//a[@class='skip-link skip-account'] // span[@class='label']"));
		drobox.click();
		WebElement logout = driver.findElement(By.xpath("//a[@title='Log Out']"));
		logout.click();

	}

	@DataProvider(name = "UserandPass")
	public Object[][] getUserPass() {
		return new Object[][] { new Object[] { "automationvalid_01@gmail.com", "111111" },

		};
	}

	@AfterClass
	public void afterClass() {
		driver.quit();

	}

}