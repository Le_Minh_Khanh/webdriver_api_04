package testAnnotation;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TC02_TestNG_Multibrowsers {
	WebDriver driver;

	@Parameters({ "browser" })
	@BeforeClass
	public void beforeClass(String browser) {
		if (browser.equals("firefox")) {
			driver = new FirefoxDriver();
		}

		else if (browser.equals("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", ".\\Driver\\chromedriver.exe");
			driver = new ChromeDriver();
		}

		driver.get("http://live.guru99.com/");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@Parameters({ "username", "pass" })
	@Test 
	public void TC01(String username, String pass) {
				WebElement account = driver.findElement(By.xpath("//div[@class='footer']//a[contains(@title,'My Account')]"));
		account.click();
		WebElement mailaddress = driver.findElement(By.xpath("//input[@id='email']"));
		mailaddress.sendKeys(username);
		WebElement passWork = driver.findElement(By.xpath("//input[@id='pass']"));
		passWork.sendKeys(pass);
		WebElement login = driver.findElement(By.xpath("//button[@id=\"send2\"]"));
		login.click();
		WebElement loginsuccess = driver.findElement(By.xpath("//h1[text()='My Dashboard']"));
		AssertJUnit.assertTrue(loginsuccess.isDisplayed());
		WebElement drobox = driver
				.findElement(By.xpath("//a[@class='skip-link skip-account'] // span[@class='label']"));
		drobox.click();
		WebElement logout = driver.findElement(By.xpath("//a[@title='Log Out']"));
		logout.click();
	}

	@AfterClass
	public void afterClass() {
		driver.quit();
	}

}
