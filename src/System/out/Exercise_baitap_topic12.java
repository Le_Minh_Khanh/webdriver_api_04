package System.out;

public class Exercise_baitap_topic12 {

	public static void main(String[] args) {
		String testing = "Automation testing Tutorial Online 123456";
//Step 1
		int dem = 0;
		char kytu = 'a';

		for (int i = 0; i <= testing.length() - 1; i++) {
			if (testing.charAt(i) == kytu) {
				dem++;
			}
			System.out.println("ky tu " + kytu + "=" + dem);
		}
		// Step 2
		boolean checktext = testing.contains(testing);
		System.out.println("chuoi co chua testing ko?" + checktext);

		// Step 3
		boolean start = testing.startsWith("testing");
		System.out.println(start);
        //Step 4
		boolean end = testing.endsWith("Online");
		System.out.println(end);
        //Step 5
		int vitri = testing.indexOf("Tutorial");
	      System.out.println("ky tu la " + vitri );
	      //Step6
	      testing = testing.replace("Online", "Offline");
	      System.out.println("new chuoi la " + testing );
	}
}
